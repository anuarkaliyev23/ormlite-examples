package kz.mathncode.anuarkaliyev23.backend.examples.ormlite;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Этот класс необходим, чтобы выделить все, связанное
 * с соединением к базе данных и настройке ORMLite
 * в отдельный фвйл
 *
 * @author Khambar Dussaliyev
 *
 * */
public class DatabaseConnectionUtils {
    private final ConnectionSource source;

    /**
     * Конструктор, принимающий строку для соединения с базой данных
     * в формате JDBC и осуществляющий соединение c базой и настройку ORMLite
     *
     * @param jdbcConnectionString - строка в формате JDBC
     *
     * */
    public DatabaseConnectionUtils(String jdbcConnectionString) {
        try {
            source = new JdbcConnectionSource(jdbcConnectionString);

            TableUtils.createTableIfNotExists(source, Student.class);
            TableUtils.createTableIfNotExists(source, StudentGroup.class);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException("Could not Initialize connection source. Connection to database is unavailable");
        }
    }

    /**
     * Обычный геттер, возвращающий настроенный в конструкторе {@link ConnectionSource}
     *
     * @return настроенный в конструкторе {@link ConnectionSource}
     * @see ConnectionSource
     * */
    public ConnectionSource getSource() {
        return source;
    }
}
