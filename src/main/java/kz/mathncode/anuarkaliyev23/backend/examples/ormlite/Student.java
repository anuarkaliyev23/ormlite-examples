package kz.mathncode.anuarkaliyev23.backend.examples.ormlite;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Простая модель студента
 *
 * @author Khambar Dussaliyev
 * */
@DatabaseTable(tableName = "students")
public class Student {
    /**
     * Первичный ключ
     * */
    @DatabaseField(id = true)
    private int id;

    /**
     * Имя
     * */
    @DatabaseField(columnName = "first_name", canBeNull = false)
    private String firstName;

    /**
     * Фамилия
     * */
    @DatabaseField(columnName = "last_name", canBeNull = false)
    private String lastName;

    /**
     * Электронная почта
     * */
    @DatabaseField
    private String email;

    /**
     * День рождения.
     *
     * dataType = DataType.SERIALIZABLE нужно, чтобы сохранять дату в виде BLOB.
     * SQLite не предоставляет типа данных для хранения даты, поэтому ORMLite должен получить инструкции относительно
     * того, как хранить данные
     * */
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private LocalDate birthday;

    /**
     * Год приема
     * */
    @DatabaseField(columnName = "entry_year")
    private int entryYear;

    /**
     * Год выпуска
     * */
    @DatabaseField(columnName = "graduation_year")
    private int graduationYear;

    /**
     * Группа студента
     * */
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private StudentGroup group;

    public Student() {
    }

    public Student(int id, String firstName, String lastName, String email, LocalDate birthday, int entryYear, int graduationYear, StudentGroup group) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
        this.entryYear = entryYear;
        this.graduationYear = graduationYear;
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public int getEntryYear() {
        return entryYear;
    }

    public void setEntryYear(int entryYear) {
        this.entryYear = entryYear;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }

    public StudentGroup getGroup() {
        return group;
    }

    public void setGroup(StudentGroup group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                entryYear == student.entryYear &&
                graduationYear == student.graduationYear &&
                Objects.equals(firstName, student.firstName) &&
                Objects.equals(lastName, student.lastName) &&
                Objects.equals(email, student.email) &&
                Objects.equals(birthday, student.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, birthday, entryYear, graduationYear);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", birthday=" + birthday +
                ", entryYear=" + entryYear +
                ", graduationYear=" + graduationYear +
                '}';
    }
}
