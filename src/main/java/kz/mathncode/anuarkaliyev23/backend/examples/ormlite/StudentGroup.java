package kz.mathncode.anuarkaliyev23.backend.examples.ormlite;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Objects;

/**
 * Простая модель группы студентов
 *
 * @author Khambar Dussaliyev
 * */
@DatabaseTable(tableName = "student_group")
public class StudentGroup {
    /**
     * Первичный ключ
     * */
    @DatabaseField(id = true)
    private int id;

    /**
     * Название группы
     * */
    @DatabaseField
    private String name;

    public StudentGroup() {
    }

    public StudentGroup(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentGroup group = (StudentGroup) o;
        return id == group.id &&
                Objects.equals(name, group.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "StudentGroup{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
