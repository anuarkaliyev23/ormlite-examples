package kz.mathncode.anuarkaliyev23.backend.examples.ormlite;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


/**
 * Класс для проведения тестов примеров кода по SQLite
 * */
class ExamplesTest {
    private Dao<Student, Integer> studentDao;
    private Dao<StudentGroup, Integer> studentGroupsDao;

    @BeforeEach
    void setUp() throws SQLException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm.ss.SSS");
        DatabaseConnectionUtils connectionUtils = new DatabaseConnectionUtils(
                String.format("jdbc:sqlite:C:\\Users\\Khambar\\Documents\\Idea Projects\\ormlite-examples\\src\\test\\resources\\test%s.sqlite", formatter.format(LocalDateTime.now())));
        studentDao = DaoManager.createDao(connectionUtils.getSource(), Student.class);
        studentGroupsDao = DaoManager.createDao(connectionUtils.getSource(), StudentGroup.class);
        prepareData();
    }

    void prepareData() throws SQLException {
        studentGroupsDao.create(STUDENT_GROUP_1);
        studentGroupsDao.create(STUDENT_GROUP_2);
        studentGroupsDao.create(STUDENT_GROUP_3);

        studentDao.create(STUDENT_1);
        studentDao.create(STUDENT_2);
        studentDao.create(STUDENT_3);
        studentDao.create(STUDENT_4);
        studentDao.create(STUDENT_5);
        studentDao.create(STUDENT_6);
    }


    @Test
    void findStudentById() throws SQLException {
        assertEquals(STUDENT_1, Examples.findStudentById(studentDao, 1));
        assertEquals(STUDENT_2, Examples.findStudentById(studentDao, 2));
        assertEquals(STUDENT_3, Examples.findStudentById(studentDao, 3));
        assertEquals(STUDENT_4, Examples.findStudentById(studentDao, 4));
        assertEquals(STUDENT_5, Examples.findStudentById(studentDao, 5));
        assertEquals(STUDENT_6, Examples.findStudentById(studentDao, 6));
    }

    @Test
    void findAllStudents() throws SQLException {
        assertEquals(Examples.findAllStudents(studentDao), List.of(STUDENT_1, STUDENT_2, STUDENT_3, STUDENT_4, STUDENT_5, STUDENT_6));
    }

    @Test
    void findByFirstName() throws SQLException {
        assertEquals(Examples.findStudentByFirstName(studentDao, "John"), List.of(STUDENT_1));
        assertEquals(Examples.findStudentByFirstName(studentDao, "James"), List.of(STUDENT_2));
        assertEquals(Examples.findStudentByFirstName(studentDao, "Jane"), List.of(STUDENT_3, STUDENT_4));
        assertEquals(Examples.findStudentByFirstName(studentDao, "June"), List.of(STUDENT_5));
        assertEquals(Examples.findStudentByFirstName(studentDao, "Jordan"), List.of(STUDENT_6));
    }

    @Test
    void deleteById() throws SQLException {
        Examples.deleteStudentById(studentDao, 1);
        assertNull(Examples.findStudentById(studentDao, 1));
    }

    @Test
    void findStudentGroupById() throws SQLException {
        assertEquals(STUDENT_GROUP_1, Examples.findStudentGroupById(studentGroupsDao, 1));
        assertEquals(STUDENT_GROUP_2, Examples.findStudentGroupById(studentGroupsDao, 2));
        assertEquals(STUDENT_GROUP_3, Examples.findStudentGroupById(studentGroupsDao, 3));
    }

    @Test
    void findAllStudentGroup() throws SQLException {
        assertEquals(
                List.of(STUDENT_GROUP_1, STUDENT_GROUP_2, STUDENT_GROUP_3),
                Examples.findAllStudentGroup(studentGroupsDao)
        );
    }



    private static final StudentGroup STUDENT_GROUP_1 = new StudentGroup(
            1,
            "Computer Science-1"
    );

    private static final StudentGroup STUDENT_GROUP_2 = new StudentGroup(
            2,
            "Mathematics-1"
    );

    private static final StudentGroup STUDENT_GROUP_3 = new StudentGroup(
            3,
            "Finance-1"
    );



    private static final Student STUDENT_1 = new Student(
            1,
            "John",
            "Doe",
            "jdoe@example.com",
            LocalDate.of(1995, 1, 1),
            2010,
            2014,
            STUDENT_GROUP_1
    );

    private static final Student STUDENT_2 = new Student(
            2,
            "James",
            "Doe",
            "james.doe@example.com",
            LocalDate.of(1996, 12, 14),
            2010,
            2014,
            STUDENT_GROUP_1
    );

    private static final Student STUDENT_3 = new Student(
            3,
            "Jane",
            "Doe",
            "jane@gmail.com",
            LocalDate.of(1998, 9, 12),
            2010,
            2014,
            STUDENT_GROUP_2
    );

    private static final Student STUDENT_4 = new Student(
            4,
            "Jane",
            "Daranoffsky",
            "jane.daranoffsky@example.com",
            LocalDate.of(1998, 3, 5),
            2010,
            2014,
            STUDENT_GROUP_2
    );

    private static final Student STUDENT_5 = new Student(
            5,
            "June",
            "Doe",
            "june@gmail.com",
            LocalDate.of(2000, 4, 28),
            2012,
            2016,
            STUDENT_GROUP_3
    );

    private static final Student STUDENT_6 = new Student(
            6,
            "Jordan",
            "Doe",
            "jordan.doe@example.com",
            LocalDate.of(2001, 1, 2),
            2012,
            2016,
            STUDENT_GROUP_3
    );


}