package kz.mathncode.anuarkaliyev23.backend.examples.ormlite;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Класс содержащий примеры кода для ORMLite
 *
 * @author Khambar Dussaliyev
 * */
public class Examples {

    /**
     * Пример сохранения объекта в базе
     * */
    static void saveStudent(Dao<Student, Integer> dao, Student student) throws SQLException {
        dao.create(student);
    }

    /**
     * Пример нахождения студента по id {@link Student#getId()}
     * */
    static Student findStudentById(Dao<Student, Integer> dao, int id) throws SQLException {
        return dao.queryForId(id);
    }

    /**
     * Пример нахождения всех студентов, находящихся в базе
     * */
    static List<Student> findAllStudents(Dao<Student, Integer> dao) throws SQLException {
        return dao.queryForAll();
    }

    /**
     * Пример нахождения студентов по их имени {@link Student#getFirstName()}
     * */
    static List<Student> findStudentByFirstName(Dao<Student, Integer> dao, String firstName) throws SQLException {
        QueryBuilder<Student, Integer> queryBuilder = dao.queryBuilder();
        PreparedQuery<Student> query = queryBuilder.where().eq("first_name", firstName).prepare();
        return dao.query(query);
    }

    /**
     * Пример удаления студента по id {@link Student#getId()}
     * */
    static void deleteStudentById(Dao<Student, Integer> dao, int id) throws SQLException {
        dao.deleteById(id);
    }

    static void saveStudentGroup(Dao<StudentGroup, Integer> dao, StudentGroup group) throws SQLException {
        dao.create(group);
    }

    /**
     * Пример сохранения объекта в базе
     * */
    static StudentGroup findStudentGroupById(Dao<StudentGroup, Integer> dao, int id) throws SQLException {
        return dao.queryForId(id);
    }

    /**
     * Пример нахождения всех групп в базе
     * */
    static List<StudentGroup> findAllStudentGroup(Dao<StudentGroup, Integer> dao) throws SQLException {
        return dao.queryForAll();
    }
}
